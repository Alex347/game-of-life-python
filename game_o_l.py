import re
import numpy as np
import os


class GameOL:
    ALIVE = 1
    DEAD = 0
    vals = [ALIVE, DEAD]

    def __init__(self, dir):
        self.FILE_DIR = dir

    def random_matrix(self, N):
        return np.random.choice(self.vals, N * N, p=[0.2, 0.8]).reshape(N, N)

    def add_pattern(self, j, i, grid, pattern):
        glider = pattern
        grid[i:i + i, j:j + j] = glider

    def update_matrix(self, frameNum, img, grid, N):
        newGrid = grid.copy()
        for i in range(N):
            for j in range(N):
                total = int((grid[i, (j - 1) % N] + grid[i, (j + 1) % N] +
                             grid[(i - 1) % N, j] + grid[(i + 1) % N, j] +
                             grid[(i - 1) % N, (j - 1) % N] + grid[(i - 1) % N, (j + 1) % N] +
                             grid[(i + 1) % N, (j - 1) % N] + grid[(i + 1) % N, (j + 1) % N]) / self.ALIVE)

                # apply Conway's rules
                if grid[i, j] == self.ALIVE:
                    if (total < 2) or (total > 3):
                        newGrid[i, j] = self.DEAD
                else:
                    if total == 3:
                        newGrid[i, j] = self.ALIVE

        # update data
        img.set_data(newGrid)
        grid[:] = newGrid[:]
        return img,

    def rle_decode(self, text):
        return re.sub(r'(\d+)(\D)', lambda m: m.group(2) * int(m.group(1)),
                      text)

    def replace_string(self, str_list, x):
        final = self.rle_decode(str_list).replace("b", "0").replace("o", "1")
        final_tab = []
        for i in list(final):
            final_tab.append(int(i))
        if x > len(final_tab):
            custom_range = x - len(final_tab)
            for i in range(custom_range):
                final_tab.append(0)

        return final_tab

    def parse_file(self, file_name):
        rel_path = os.path.join(self.FILE_DIR, file_name)
        f = open(rel_path, "r")
        val = f.read()
        x = re.search("x = (.?)", val).group(0).split("=")[-1]
        y = re.search("y = (.?)", val).group(0).split("=")[-1]
        rle = re.search("(.*?)!", val).group(1).split("$")
        return x, y, rle

    def retrieve_pattern(self, x, y, rle):
        grid = []
        x = int(x)
        y = int(y)
        for item in rle:
            grid.append(self.replace_string(item, x))

        data = {"x": x, "y": y, "pattern": grid}
        return data
