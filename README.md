# Game of life python
Game of life with file parsing(.rle)

# Installation
`pip install virtualenv`

`virtualenv your_venv`

`source your_venv/bin/activate`   # linux
`your_venv\Scripts\activate`  # windows

`pip install -r requirements.txt`

you can use args in cmd:
       ` python main.py --grid=50 --file=worm`
    or random:
        `python main.py --grid=50 --file=random`
    or nothing:
        `python main.py`



