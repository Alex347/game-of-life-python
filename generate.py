import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import os
import pickle
from game_o_l import GameOL


class Generate(GameOL):
    def __init__(self, dir):
        super().__init__(dir)

    def parse_files(self):
        files_dir = os.listdir(self.FILE_DIR)
        files_list = []
        for file in files_dir:
            if file.endswith(".rle"):
                files_list.append(file)

        """ parsing files """
        for i in files_list:
            parsing = self.parse_file(i)
            data = self.retrieve_pattern(parsing[0], parsing[1], parsing[2])
            pickle.dump(data, open(f"exports\\{i}", "wb"))  # binary dump

    def open_file(self, file):
        pickle_in = open(f"exports\\{file}.rle", "rb")
        pickle_dict = pickle.load(pickle_in)
        x = pickle_dict["x"]
        y = pickle_dict["y"]
        pattern = pickle_dict["pattern"]
        return {"x": x, "y": y, "pattern": pattern}

    def animate(self, grid, N):
        fig, ax = plt.subplots()
        img = ax.imshow(grid, interpolation='None')
        ani = animation.FuncAnimation(fig, self.update_matrix,
                                      fargs=(img, grid, N,),
                                      # frames=10,
                                      interval=100,
                                      save_count=50)

        plt.show()

    def show_plt(self, N, file):
        if file == "random":
            grid = self.random_matrix(N)
        else:
            self.parse_files()
            data = self.open_file(file)
            grid = np.zeros(N * N).reshape(N, N)
            self.add_pattern(data["x"], data["y"], grid, data["pattern"])

        self.animate(grid, N)
