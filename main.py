import argparse
from generate import Generate
import os

FILE_DIR = os.path.join(os.path.dirname(__file__), "files")


def main():
    """
    you can use args in cmd:
        py main.py --grid=50 file=worm
    or random:
        py main.py --grid=50 file=random
    or nothing:
        py main.py
    """
    ap = argparse.ArgumentParser(add_help=False)
    ap.add_argument('-g', '--grid', help='grid size', default=50)
    ap.add_argument('-f', '--file', help='file', default="glider")
    args = vars(ap.parse_args())
    generate = Generate(FILE_DIR)
    generate.show_plt(int(args["grid"]), args["file"])


if __name__ == '__main__':
    main()
